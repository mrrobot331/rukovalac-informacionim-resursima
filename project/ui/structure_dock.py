from PySide2 import QtCore, QtWidgets, QtGui

class StructureDock(QtWidgets.QDockWidget):
    kliknut = QtCore.Signal(str) #klasni atribut

    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.model = QtWidgets.QFileSystemModel()
        self.main_window = parent

        skip = ["*.csv"]

        
        self.model.setRootPath(QtCore.QDir.currentPath())
        self.model.setFilter(QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllEntries)
        self.model.setNameFilters(skip)
        self.model.setNameFilterDisables(False)


        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + "/data"))
        self.tree.clicked.connect(self.file_clicked)
        self.setWidget(self.tree)

    def file_clicked(self, index):
        file_path = self.model.filePath(index)
        data_filepath = file_path.replace("_metadata.json", "_data.csv")
        print(data_filepath)
        self.main_window.open_new_file(data_filepath)
        print(self.model.filePath(index)) #printujemo path od index-a

    
    def set_root_path(self, path):
        self.model.setRootPath(path)
        self.tree.setRootIndex(self.model.index(path))
    