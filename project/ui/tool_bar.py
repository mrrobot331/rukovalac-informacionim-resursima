from PySide2 import QtCore, QtWidgets, QtGui

class ToolBar(QtWidgets.QToolBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent

        #instanciranje akcija
        save_action = QtWidgets.QAction("Save", self, 
            shortcut=None, 
            statusTip="Open",
            triggered=self.main_window.save)

        insert_action = QtWidgets.QAction("Insert", self, 
            shortcut=None, 
            statusTip="Open")

        delete_action = QtWidgets.QAction("Delete", self, 
            shortcut=None, 
            statusTip="Open")

        #populate toolbar
        self.addAction(save_action)
        self.addAction(insert_action)
        self.addAction(delete_action)

    