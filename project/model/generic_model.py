from PySide2 import QtCore
import json
class GenericModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None, metadata = None):
        super().__init__(parent)
        self.elements = []
        self.metadata = metadata
        
    
    def get_element(self, index):
        return self.elements[index.row()]

    def handler_type_check(self):
        with open(self.meta_filepath) as metadata:
            data = json.load(metadata)
            self.meta_data = data
            if data["type"] == "serijska":
                return "serijska"
            elif data["type"] == "sekvencijalna":
                return "sekvencijalna"

    def rowCount(self, index):
        return len(self.elements)
        
    def columnCount(self, index):
        return len(self.metadata['columns'])

    def data(self, index, role = QtCore.Qt.DisplayRole):
        element = self.get_element(index)
        e = 0
        length = len(element)
        while e < length:
            if index.column() == e and role == QtCore.Qt.DisplayRole:
                return element[e]
                print(element[e])
            e+=1
        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.metadata['column_names'][section]

        return None

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        element = self.get_element(index)
        print(element)
        if value == " ":
            return False
        if role == QtCore.Qt.EditRole:
           #element[self.metadata['columns'][index.column()]] = value
           #setattr(element,element['columns'][index.column()],value)
           element[index.column()]=value
           print(element)
           return True

    

    def flags(self, index):
        return super().flags(index) | QtCore.Qt.ItemIsEditable