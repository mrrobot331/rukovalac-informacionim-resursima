class Studenti():
    def __init__(self, ustanova="", struka="", broj_indeksa="", prezime="", ime_roditelja="", ime="", pol="", adresa_stanovanja="", telefon="", jmbg="", datum_rodjenja=""):
        self.ustanova = ustanova
        self.struka = struka
        self.broj_indeksa = broj_indeksa
        self.prezime = prezime
        self.ime_roditelja = ime_roditelja
        self.ime = ime
        self.pol = pol
        self.adresa_stanovanja = adresa_stanovanja
        self.telefon = telefon
        self.jmbg = jmbg
        self.datum_rodjenja = datum_rodjenja

