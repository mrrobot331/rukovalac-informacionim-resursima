from model.studenti import Studenti
from model.visokoskolska_ustanova import VisokoskolskaUstanova
from model.studijski_program import StudijskiProgram
import csv
from handlers.serial_file_handler import SerialFileHandler

def ispisi_studente(studenti, ustanove, programi):
    for student in studenti:
        print("Indeks: {}, Ime: {}, Prezime: {}".format(student.broj_indeksa, student.ime, student.prezime))
        print("Ustanove: ")

        for ustanova in ustanove:
            for i in range(len(student.ustanove)):
                if(student.ustanove[i] == ustanova.oznaka):
                    print("-", ustanova.naziv)

                    for j in range(len(ustanova.programi)):
                        for program in programi:
                            if(ustanova.programi[j] == programi.ustanova):
                                print("Programi: ", programi.naziv_programa, programi.oznaka_programa)

        print("------------------------")

s_data = []
s_data.append(Studenti("Singidunum", "Student", "2018/123", "Jelic", "Slavko", "Ivan", "Musko", "Adresa 10", "7890", "4566378", "Datum1"))
s_data.append(Studenti("FTN", "Student", "2016/456", "Vidic", "Branko", "Nemanja", "Musko", "Adresa 50", "3456", "647485", "Datum2"))
s_data.append(Studenti("Fimek", "Student", "2020/875", "Grande", "Pablo", "Ariana", "Zensko", "Adresa 77", "6789", "4512343", "Datum3"))


u_data = []
u_data.append(VisokoskolskaUstanova("Oznaka 1", "Singidunum", "Adresa 20"))
u_data.append(VisokoskolskaUstanova("Oznaka 2", "Fimek", "Adresa 26"))
u_data.append(VisokoskolskaUstanova("Oznaka 3", "FTN", "Adresa 68"))


p_data = []
p_data.append(StudijskiProgram("Singidunum", "A", "SPA", "Strukture podataka"))
p_data.append(StudijskiProgram("FTN", "E", "E2", "Web programiranje"))
p_data.append(StudijskiProgram("Fimek", "Z", "ZUR", "Zurnalistika"))


with open("student_data.csv", 'w', newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar="'")
    for student in s_data:
        writer.writerow([student.broj_indeksa, student.ime, student.prezime])

with open("visokoskolska_ustanova_data.csv", 'w', newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar="'")
    for ustanova in u_data:
        writer.writerow([ustanova.naziv, ustanova.oznaka, ustanova.adresa])

with open("studijski_programi_data.csv", 'w', newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar="'")
    for program in p_data:
        writer.writerow([program.ustanova, program.nivo, program.oznaka_programa])


student_file_handler = SerialFileHandler("studenti_data.csv", "data/studenti_metadata.json")

studenti = [Studenti("Singi", "Student", "2018/123", "Jelici", "Slavkoni", "Ivani", "Zensko", "Adresa 11", "7895", "4566358", "Datum2"), Studenti("Singidunum", "Student", "2018/123", "Jelic", "Slavko", "Ivan", "Musko", "Adresa 10", "7890", "4566378", "Datum1")]
student_file_handler.insert(Studenti("Singi", "Student", "2018/777", "Ivanovic", "Mirko", "Branko", "Zensko", "Adresa 15", "1111", "22222", "Datum5"))
student_file_handler.edit("2018/123", Studenti("Singi", "Ucenik", "2018/123", "Jelici", "Slavkoni", "Ivani", "Zensko", "Adresa 11", "7895", "4566358", "Datum2"))
student_file_handler.insert_many(studenti)

ustanova_file_handler = SerialFileHandler("visokoskolska_ustanova_data.csv", "data/visokoskolska_ustanova_metadata.json")


program_file_handler = SerialFileHandler("studijski_programi_data.csv", "data/studijski_programi_metadata.json")


studenti = student_file_handler.get_all()
ustanove = ustanova_file_handler.get_all()
programi = program_file_handler.get_all()

ispisi_studente(studenti, ustanove, programi)
